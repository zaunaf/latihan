<?php
/*
 * Aplikasi Latihan
 * 
 */

//print_r($_REQUEST); die;
require('startup.php');
//error_reporting(E_ALL);

//$requestURI = explode('/', $_SERVER['REQUEST_URI']);
//print_r($requestURI); die;

//Tahun//
$c = new Criteria();
$c->add(TahunPeer::NAMA, NAMA_TAHUN);
$tahun_obj = TahunPeer::doSelectOne($c);
$tahun_id = $tahun_obj->getTahunId();

//getValueBySql('select nama from tahun;');

//Auth//
$auth = new XondAuth();
$auth->setAuthObject('Pengguna');
$auth->setUserColumn('Username');
$auth->setPasswordColumn('Password');
$auth->setRedirectUrl("/");
$auth->addGroupMembership('JabatanId');
//$auth->registerUi('examples',$auth->SYSDIR);

$app = new XondApplication();
$app->registerNoAuthModule('PencetakanSpp');
$app->registerNoAuthModule('Network');
$app->registerNoAuthModule('SpmNetwork');
$app->registerNoAuthModule('Mobile');
$app->registerNoAuthModule('Visual');
$app->registerNoAuthModule('Realisasi');
$app->registerNoAuthAction('print');

if ($_SERVER["argc"] == 4) {
	$_REQUEST["m"] = $_SERVER["argv"][1];
	$_REQUEST["a"] = $_SERVER["argv"][2];
	$_REQUEST["t"] = $_SERVER["argv"][3];
	$_GET["m"] = $_SERVER["argv"][1];
	$_GET["a"] = $_SERVER["argv"][2];
	$_GET["t"] = $_SERVER["argv"][3];	
} 

$app->registerParentMenu('Master Brur', 1);
$app->registerParentMenu('Laporan', 2);

/*
if ($_REQUEST['a'] == 'print') {
	
	//don't set auth
	
} else {
*/
//$app->setAuth($auth);
//print_r($_SESSION); die;
//if ($app->getAuth()->getSession()) {
	
if ($app->setAuth($auth))  {

	if ($app->getAuth()->getSession()) {		
		$user = $app->getAuth()->getUser();	
		switch ($user->getJabatanId()) {
			case 1 :
				//$app->registerModule('DaftarPermohonanSpp');
				//$app->registerModule('PermohonanSpp');
				//$app->registerModule('Pengguna');
				//$app->registerModule('Penerima');
				//$app->registerModule('Kontrak');
				$app->registerModule('Penerima');
				$app->registerModule('KontrakSpm');
				$app->registerModule('SubOutput');				
				//$app->registerModule('PersetujuanSpp');
				//$app->registerModule('PencetakanSpp');
				//$app->registerModule('Revisi');
				//$app->registerModule('PengajuanRevisi');
				//$app->registerModule('PermohonanTup');
				//$app->registerModule('ManajemenUp');
				//$app->registerModule('PermohonanSpp');
				$app->registerModule('Routerboard');
				$app->registerModule('FeedBack');
				$app->registerModule('RealisasiPerBulan');
				$app->registerModule('RealisasiPerKategori');				
				$app->registerModule('RealisasiDipa');
				$app->registerModule('RealisasiDetil');				
				break;
			case 3 :
				//$app->registerModule('DaftarPermohonanSpp');
				//$app->registerModule('PermohonanSpp');
				////$app->registerModule('Penerima');
				////$app->registerModule('Kontrak');
				//$app->registerModule('SubOutput');
				//$app->registerModule('PersetujuanSpp');
				//$app->registerModule('PencetakanSpp');
				//$app->registerModule('Revisi');
				//$app->registerModule('PengajuanRevisi');
				//$app->registerModule('PermohonanTup');
				//$app->registerModule('ManajemenUp');
				//$app->registerModule('PermohonanSpp');
				//$app->registerModule('Routerboard');
				$app->registerModule('FeedBack');
				$app->registerModule('RealisasiPerBulan');
				$app->registerModule('RealisasiPerKategori');
				$app->registerModule('RealisasiDipa');
				$app->registerModule('RealisasiDetil');
				break;
			case 5 : //PPK Kegiatan
				//$app->registerModule('PermohonanSppUp');
				//$app->registerModule('PersetujuanSpp');
				//$app->registerModule('PencetakanSpp');
				//$app->registerModule('Revisi');
				////$app->registerModule('Penerima');
				////$app->registerModule('Kontrak');			
				//$app->registerModule('PermohonanTup');
				//$app->registerModule('PermohonanSpp');
				//$app->registerModule('Routerboard');
				$app->registerModule('Portlet');
				$app->registerModule('FeedBack');
				$app->registerModule('RealisasiPerBulan');
				$app->registerModule('RealisasiPerKategori');
				$app->registerModule('RealisasiDipa');
				$app->registerModule('RealisasiDetil');
				//$app->registerModule('ManajemenTup');
				break;				
			case 7 : //BP?
				//$app->registerModule('PermohonanSppUp');
				//$app->registerModule('PersetujuanSpp');
				//$app->registerModule('PencetakanSpp');
				//$app->registerModule('Revisi');
				$app->registerModule('Penerima');
				$app->registerModule('KontrakSpm');
				//$app->registerModule('Penerima');
				//$app->registerModule('Kontrak');
				$app->registerModule('SubOutput');
				$app->registerModule('PermohonanRevisi');
				$app->registerModule('PermohonanTup');
				$app->registerModule('ManajemenUp');
				$app->registerModule('PermohonanSpp');
				$app->registerModule('Routerboard');
				$app->registerModule('FeedBack');
				$app->registerModule('RealisasiPerBulan');
				$app->registerModule('RealisasiPerKategori');				
				$app->registerModule('RealisasiDipa');
				$app->registerModule('RealisasiDetil');
				$app->registerModule('TampilanBuku');
				//$app->registerModule('DataTransaksi');
				break;				
			case 17 : //BP
				//$app->registerModule('PermohonanSppUp');
				//$app->registerModule('PersetujuanSpp');
				//$app->registerModule('PencetakanSpp');
				//$app->registerModule('Revisi');
				$app->registerModule('Penerima');
				$app->registerModule('KontrakSpm');				
				//$app->registerModule('Penerima');
				//$app->registerModule('Kontrak');
				//$app->registerModule('Kegiatan');
				$app->registerModule('PermohonanRevisi');
				$app->registerModule('PermohonanTup');
				$app->registerModule('ManajemenUp');
				$app->registerModule('PermohonanSpp');
				$app->registerModule('Routerboard');
				$app->registerModule('FeedBack');
				$app->registerModule('RealisasiPerBulan');
				$app->registerModule('RealisasiPerKategori');				
				$app->registerModule('RealisasiDipa');
				$app->registerModule('RealisasiDetil');
				//$app->registerModule('TampilanBuku');
				//$app->registerModule('DataTransaksi');
				break;		
			case 8 : //BP
				//$app->registerModule('PermohonanSppUp');
				//$app->registerModule('PersetujuanSpp');
				//$app->registerModule('PencetakanSpp');
				//$app->registerModule('Revisi');
				$app->registerModule('Portlet');
				$app->registerModule('Penerima');
				$app->registerModule('KontrakSpm');				
				//$app->registerModule('Penerima');
				//$app->registerModule('Kontrak');
				$app->registerModule('SubOutput');				
				//$app->registerModule('Kegiatan');
				$app->registerModule('PermohonanTup');
				$app->registerModule('ManajemenUp');
				$app->registerModule('PermohonanSpp');
				$app->registerModule('DataTransaksi');
				$app->registerModule('TampilanBuku');
				$app->registerModule('RealisasiPerBulan');
				$app->registerModule('RealisasiPerKategori');				
				$app->registerModule('RealisasiDipa');
				$app->registerModule('RealisasiDetil');				
				$app->registerModule('Routerboard');
				$app->registerModule('FeedBack');
				//$app->registerModule('ManajemenTup');
				break;			
			case 9 : //BPP
				//$app->registerModule('DaftarPermohonanSpp');
				//$app->registerModule('PermohonanSpp');				
				//$app->registerModule('Revisi');
				//$app->registerModule('AcaraKegiatan');
				$app->registerModule('Portlet');
				$app->registerModule('Penerima');
				$app->registerModule('KontrakSpm');	
				$app->registerModule('ManajemenPengguna');
				//$app->registerModule('Penerima');
				//$app->registerModule('PenerimaRinci');				
				//$app->registerModule('TPegawai');
				$app->registerModule('Aktivitas');
				//$app->registerModule('Kontrak');
				$app->registerModule('PermohonanRevisi');
				$app->registerModule('PermohonanTup');
				$app->registerModule('PermohonanSpp');
				//$app->registerModule('RincianSpp');
				$app->registerModule('DataTransaksi');
				$app->registerModule('TampilanBuku');
				$app->registerModule('RealisasiDipa');
				$app->registerModule('RealisasiDetil');
				$app->registerModule('Routerboard');
				$app->registerModule('FeedBack');
				$app->registerModule('ManajemenDokumen');
				break;
			case 20 : //STAF BAGKEU
				$app->registerModule('ManajemenDokumen');
				$app->registerModule('FeedBack');
				break;				
			default :
				$app->registerModule('Routerboard');
				$app->registerModule('FeedBack');			   
		}
		
		$app->setUserInfo(array(
			'Id' => $user->getPenggunaId(),
			'Nama Akun' => $user->getUsername(),
			'Password' => str_repeat('*', strlen($user->getPassword())), 
			'Nama' => $user->getNama(),
			'NIP' => $user->getNip(),
			'Jabatan' => $user->getJabatan()->getNama()						
		));
		
		global $spinUserObj, $spinJabatanObj, $spinSatkerObj;
		//$spinUser = new Pengguna();
		//$spinJabatan = new Jabatan();
		//$spinSatker = new Satker();
		$spinUser = $app->getAuth()->getUser();
		$spinJabatan = @$spinUser->getJabatan();
		$spinSatker = @$spinUser->getSatker();
		
	}
	
}
/*
}
*/
//$app->registerModule('Bpp');
//$app->registerModule('Operator');
$libDir = $app->LIBDIR;
$sysDir = $app->SYSDIR;
$cssDir = $app->CSSDIR;
$app->registerUi('components',$sysDir);

$app->registerStyle('Portal',$libDir);
$app->registerStyle('GridSummary',$libDir);
$app->registerStyle('file-upload',$libDir);
$app->registerStyle('ProgressColumn',$libDir);
$app->registerStyle('grid-examples',$libDir);
$app->registerStyle('summary',$libDir);
$app->registerStyle('sppsai',$sysDir);
//$app->registerStyle('TreeGrid',$libDir);
//$app->registerStyle('TreeGridLevels',$libDir);
$app->registerStyle('extensible-all',$cssDir);
$app->registerStyle('calendar',$cssDir);
$app->registerStyle('calendar-app',$cssDir);
$app->registerStyle('calendar-examples',$cssDir);
$app->registerStyle('calendar-examples',$cssDir);
//$app->registerStyle('TreeGrid',$libDir);
//$app->registerStyle('TreeGridLevels',$libDir);
$app->registerStyle('TreeGrid',$cssDir);
$app->registerStyle('TreeGridLevels',$cssDir);



$app->registerUi('FeedGrid',$libDir);
$app->registerUi('GridSummary',$libDir);
$app->registerUi('FileUploadField',$libDir);
$app->registerUi('ProgressColumn',$libDir);
$app->registerUi('groupcombo',$libDir);
$app->registerUi('groupdataview',$libDir);
$app->registerUi('GroupSummary',$libDir);
$app->registerUi('example',$sysDir);
$app->registerUi('uxfusionpak',$libDir);
$app->registerUi('uxfusionpak',$libDir);
$app->registerUi('extensible-all-debug',$libDir);
$app->registerUi('ext-lang-id',$libDir);
$app->registerUi('extensible-lang-id',$libDir);
$app->registerUi('ux-all-debug',$libDir);
$app->registerUi('Ext.tree.TreeSerializer', $libDir);
$app->registerUi('TreeGrid', $libDir);



//$app->registerUi('uxmedia-min',$libDir);
//$app->registerUi('uxflash-min',$libDir);
//$app->registerUi('uxfusion',$libDir);
/*
global $spinUserObj, $spinJabatanObj, $spinSatkerObj;
$spinUser = $app->getAuth()->getUser();
$spinJabatan = $spinUser->getJabatan();
$spinSatker = $spinUser->getSakter();
*/
$app->setDevelStatus(true);
$app->start();
$app->render();
//echo "m = {$_GET['m']}, a = {$_GET['a']}, t = {$_GET['t']}"; die;

//Set Globals//


?>